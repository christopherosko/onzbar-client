import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import { Appbar, Button, TextInput, Title } from 'react-native-paper';

class SignInComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email: '',
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <Appbar.Header>
            <Appbar.Content
                title="STONED"
                subtitle="Mot de passe oublié"
            />
      </Appbar.Header>
        <View style={styles.top}>
          <Title style={styles.errorMessage}>{ this.state.errorMessage }</Title>
          <TextInput
            style={styles.text}
            label='Email'
            value={this.state.email}
            onChangeText={text => this.setState({ email: text })}
          />
            <Button style={styles.button} mode="contained" onPress={() => this.login()}> 
                Send E-mail
            </Button> 
        </View>
        <View style={styles.top}>
        {/* Mot de passe oublié a gérer */}
 
        </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white'
  },
  top: {
    marginTop: 100,
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  center: {
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  bottom: {
    width: '100%',
    height: '33.33%',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  text: {
    marginTop: 100,
    width: 300,
    height: 50,
  },
  button: {
    marginTop: 20,
  },
  errorMessage: {
    color: 'red'
  }
});

export default SignInComponent;
