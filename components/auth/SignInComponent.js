import React, { Component } from 'react';
import base64 from 'react-native-base64';
import { View, StyleSheet, AsyncStorage, Image } from 'react-native';
import { Appbar, Button, TextInput, Title } from 'react-native-paper';
import urlApi from '../../assets/api';
import * as axios from 'axios';

class SignInComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
      email: '',
      password: '',
    };
  }

  _signIn = () => {
    let encoded = base64.encode(this.state.email.toLowerCase() + ':' + this.state.password.toLowerCase());
    let axiosConfig = {
      headers: {
          'Content-Type': 'application/json',
          Authorization: 'Basic ' + encoded
      }
    };
    axios.get(urlApi + 'users/login', axiosConfig)
    .then((response) => {
      this._storeData(response.data.token.token);
      this.props.navigation.navigate('Home');
    })
    .catch((error) => {
      console.log("🚫" + error);
      this.setState({
        errorMessage: 'Problèmes de connexion'
      });
    });
  }

  _storeData = (token) => {
    try {
      AsyncStorage.setItem('@token', token)
    } catch (error) {
     console.error("🚫" + error);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Appbar.Header style={styles.header}>     
                <View style={styles.vue} />                         
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>
                            
                    </Image>
                <View style={styles.vue} />
        </Appbar.Header>
        <View style={styles.top}>
          <Title style={styles.errorMessage}>{ this.state.errorMessage }</Title>
          <TextInput
            style={styles.text}
            label='Email'
            value={this.state.email}
            onChangeText={text => this.setState({ email: text })}
            underlineColor='black'
            selectionColor='black'
          />
          <TextInput
            style={styles.text}
            label='Mot de passe'
            value={this.state.password}
            onChangeText={text => this.setState({ password: text })}
            secureTextEntry={true}
            underlineColor='black'
            selectionColor='black'
          />
    </View>
    <View style={styles.center}>
      <Button mode="contained" onPress={() => this._signIn()} color='black'>
        Se connecter
      </Button>
    </View>
    <View style={styles.bottom}>
      <Button mode="outlined" onPress={() => this.props.navigation.navigate('SignUp')} color='black'>
        S'inscrire
      </Button>

    </View>
  </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white'
  },
  top: {
    marginTop: 100,
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  center: {
    width: '100%',
    height: '33.33%',
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 20
  },
  bottom: {
    width: '100%',
    height: '33.33%',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  text: {
    marginTop: 20,
    width: 300,
    height: 50,
    borderColor: 'black'
  },
  header: {
    width: '100%',
    backgroundColor: 'black',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logo: {
      height: 40,
      width: '36%',
  },
  vue: {
      height: '20%',
      width: '10%',
  },
  errorMessage: {
    color: 'red'
  }
});

export default SignInComponent;
