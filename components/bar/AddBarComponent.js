import React, { Component } from 'react'
import { ScrollView, View, Image, StyleSheet, AsyncStorage } from 'react-native'
import { Appbar, TextInput, Button, Headline } from 'react-native-paper'
// import { ScrollView } from 'react-native-gesture-handler'
import urlApi from '../../assets/api'
import * as axios from 'axios'

export class ModifBarComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            adress: '',
            openhour: '',
            happyhour: '',
            token: ''

        };
        this._retrieveData();
    }

    htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    _retrieveData = async () => {
        try {
            const token = await AsyncStorage.getItem('@token');
            if (token !== null) {
                this.setState({ token })
            }
        } catch (error) {
            console.error("🚫" + error);
        }
    };

    _RegisterBar = () => {
        console.log(this.state);
        const data = this.state;
        if (data.name !== "" && data.description !== "" && data.adress !== "" && data.openhour !== "" && data.happyhour !== "") {
            const newBar = {
                "name": this.htmlEntities(data.name),
                "description": this.htmlEntities(data.description),
                "adress": this.htmlEntities(data.adress),
                "encoded_loc": encodeURIComponent(data.adress.trim()),
                "openhour": this.htmlEntities(data.openhour),
                'happyhour': this.htmlEntities(data.happyhour)
            };

            let axiosConfig = {
                headers: {
                    'Content-Type': 'application/json',
                    'x-access-token': this.state.token
                }
            };
            axios.post(urlApi + 'bars/', newBar, axiosConfig)
                .then((response) => {
                    this.props.navigation.navigate('Home');
                })
                .catch(function (error) {
                    console.log("🚫" + error);
                });
        } else {
            this.setState({ errorMessage: 'Vous n\'avez pas tous rempli' })
        }
    }

    render() {
        return (
            <View style={styles.page}>
                <Appbar.Header style={styles.header}>
                    <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
                    <Image
                        source={require('../../assets/logo.png')}
                        style={styles.logo}>
                    </Image>
                    <View style={styles.vue} />
                </Appbar.Header>

                <View style={styles.parent}>
                    <View style={styles.child0}>
                        <Headline style={{ fontWeight: 'bold', textDecorationLine: 'underline', fontSize: 30, flex: 1, justifyContent: 'center', flexDirection: 'column' }}>
                            Ajouter un bar
                        </Headline>
                    </View>
                    <ScrollView style={{ margin: '-10%' }}>
                        <View style={styles.child1}>
                            <TextInput
                                style={styles.text}
                                label='Nom'
                                value={this.state.name}
                                onChangeText={text => this.setState({ name: text })}
                            />
                            <TextInput
                                style={styles.text}
                                label='Adresse'
                                value={this.state.adress}
                                onChangeText={text => this.setState({ adress: text })}
                            />
                            <TextInput
                                style={styles.longText}
                                label='Description'
                                value={this.state.description}
                                multiline
                                onChangeText={text => this.setState({ description: text })}
                            />
                            <TextInput
                                style={styles.text}
                                label='Horaires Ouverture / Fermeture'
                                value={this.state.openhour}
                                onChangeText={text => this.setState({ openhour: text })}
                            />
                            <TextInput
                                style={styles.text}
                                label='Horaires Happy Hour'
                                value={this.state.happyhour}
                                onChangeText={text => this.setState({ happyhour: text })}
                            />
                            <View style={{ height: 100 }} />
                        </View>
                    </ScrollView>
                    <View style={styles.child2}>
                        <View style={styles.litleChild1}>
                            <Button style={styles.button} mode="contained" onPress={() => this.props.navigation.goBack()} color='black' >
                                Annuler
                        </Button>
                        </View>
                        <View style={styles.litleChild2}>
                            <Button style={styles.button} mode="contained" onPress={() => this._RegisterBar()} color='black' >
                                Confirmer
                        </Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        width: '100%',
        height: '100%',
        position: 'absolute',
    },
    logo: {
        height: 40,
        width: '36%',
    },
    header: {
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    vue: {
        height: '20%',
        width: '10%',
    },
    parent: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
    },
    child0: {
        paddingTop: '3%',
        width: '100%',
        height: '12%',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    child1: {
        flex: 1,
        width: '100%',
        height: '25%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    child2: {
        width: '100%',
        height: 200,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row'
    },
    litleChild1: {
        width: 150,
        height: 10,
    },
    litleChild2: {
        width: 150,
        height: 10,
    },
    text: {
        marginTop: 20,
        width: 300,
        height: 60
    },
    longText: {
        marginTop: 20,
        width: 300,
        height: 180
    },
    button: {
        height: 60,
        justifyContent: 'center'
    }
})

export default ModifBarComponent
