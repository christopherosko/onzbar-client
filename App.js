import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, Text, View } from 'react-native';
import HomeComponent from './components/HomeComponent';
import SignInComponent from './components/auth/SignInComponent';
import SignUpComponent from './components/auth/SignUpComponent';
import ForgotPasswordComponent from './components/auth/ForgotPasswordComponent';
import NewPasswordComponent from './components/auth/NewPasswordComponent';
import AddBarComponent from './components/bar/AddBarComponent'
import ModifBarComponent from './components/bar/ModifBarComponent'
import DetailBarComponent from './components/bar/DetailBarComponent';
import FavBarComponent from './components/bar/FavBarComponent';
import DetailUserComponent from './components/user/DetailUserComponent';
import DrinksComponent from './components/bar/DrinksComponent';
import QrCodeComponent from './components/qr_code/QrCodeComponent'

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SignIn"
        screenOptions={{
          headerShown: false
        }}
      >
        <Stack.Screen name="Home" component={HomeComponent} />
        <Stack.Screen name="SignIn" component={SignInComponent} />
        <Stack.Screen name="SignUp" component={SignUpComponent} />
        <Stack.Screen name="ForgotPassword" component={ForgotPasswordComponent} />
        <Stack.Screen name="NewPassword" component={NewPasswordComponent} />
        <Stack.Screen name="DetailBar" component={DetailBarComponent} />
        <Stack.Screen name="AddBar" component={AddBarComponent} />
        <Stack.Screen name="ModifBar" component={ModifBarComponent} />
        <Stack.Screen name="FavBar" component={FavBarComponent} />
        <Stack.Screen name="DetailUser" component={DetailUserComponent} />
        <Stack.Screen name="Drinks" component={DrinksComponent} />
        <Stack.Screen name="QrCode" component={QrCodeComponent} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
